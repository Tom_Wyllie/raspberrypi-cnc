BOARD = 1
BCM = 1
OUT = 'out'
IN = 'in'
LOGGING = False


def setmode(a):
    log(a)


def setup(a, b):
    log('Setting pin {:d} to {}'.format(a, b))


def output(a, b):
    log('Setting pin {:d} to {}'.format(a, str(b)))


def cleanup():
    log('Cleaning up pins')


def setwarnings(flag):
    log('Settings warnings {}'.format(str(flag)))


def log(text):
    if LOGGING:
        print('[GPIO] {}'.format(text))
