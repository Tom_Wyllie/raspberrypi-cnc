import os

# Raspberry Pi uses its own GPIO package, use a dummy from the local directory otherwise
if os.uname()[1] == 'Ariel':    # The Raspberry Pi controlling the CNC is called Ariel
    # noinspection PyUnresolvedReferences
    import RPi.GPIO as GPIO
    # noinspection PyUnresolvedReferences
    from laser_cnc import LaserCNC as CNCMachine
else:
    # noinspection PyUnresolvedReferences
    import GPIO
    # noinspection PyUnresolvedReferences
    from dummy_cnc import DummyCNC as CNCMachine
