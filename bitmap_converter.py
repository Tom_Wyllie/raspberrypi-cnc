#!/usr/bin/env python
import argparse

from PIL import Image
import numpy as np

import numpy_utils


def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('image', help='The specified image to convert to .tara machine code')
    args = arg_parser.parse_args()

    image = Image.open(args.image, 'r')
    img_matrix = np.asarray(image.convert('L'))

    inverse_matrix = 255 - img_matrix
    inverse_matrix = numpy_utils.trim_zeros(inverse_matrix)

    # The row and column of every non zero item in the image
    nz_rows, nz_columns = np.nonzero(inverse_matrix)

    # Get the indices of the first and last occurrence of each row
    _, first_row_index = np.unique(nz_rows, return_index=True)
    _, last_row_index = np.unique(nz_rows[::-1], return_index=True)
    last_row_index = nz_rows.size - (1 + last_row_index)

    # Use the indices from each row to get the indices of the first and last column of each row
    trim_indices = np.vstack((nz_columns[first_row_index], nz_columns[last_row_index] + 1)).T

    # Reinsert necessary whitespace to pad rows out to where they originally were
    for i, func in enumerate((np.min, np.max)):
        columns = trim_indices[1 - i::2, i], trim_indices[2 - i::2, i]
        num_pairs = min(columns[0].size, columns[1].size)
        trimmed_columns = columns[0][:num_pairs], columns[1][:num_pairs]
        edges = func(np.vstack(trimmed_columns), axis=0)
        trim_indices[1 - i::2, i][:num_pairs], trim_indices[2 - i::2, i][:num_pairs] = edges, edges

    # Trim all outside the first and last indices for each row, to create a list of row arrays
    img_matrix = [255 - inverse_matrix[i][low:high] for i, (low, high) in enumerate(trim_indices)]

    head_size = 40
    intensity_template = 'l {:3.2f}'
    y_forward = 'y {:d}'.format(head_size)
    x_step_buffer = 0
    last_intensity = 0.

    for row in range(len(img_matrix)):
        row_array = img_matrix[row]
        if row % 2 == 1:
            row_array = row_array[::-1]

        for column in range(len(row_array)):
            pixel = row_array[column]

            intensity = intensity_from_pixel(pixel)
            if intensity != last_intensity:
                # Debuffer x
                if x_step_buffer:
                    print('x {:d}'.format(x_step_buffer))
                    x_step_buffer = 0

                print(intensity_template.format(intensity))
                last_intensity = intensity

            if column < len(row_array):
                direction = -1 if row % 2 else 1
                x_step_buffer += direction * head_size

        if x_step_buffer:
            print('x {:d}'.format(x_step_buffer))
            x_step_buffer = 0
        print(intensity_template.format(0.0))
        last_intensity = 0.0
        print(y_forward)


def intensity_from_pixel(pixel):
    """Get the relevant motor speed and laser modulation for a given pixel"""
    threshold_pixel = 245

    if pixel >= threshold_pixel:
        return 0
    else:
        # scale pixel from 0. (light) - 1. (dark)
        pixel = (255. - pixel) / 255.

        # Rescale PWM signal into range 0.2 - 0.9
        return 0.2 + 0.7 * pixel

if __name__ == '__main__':
    main()
