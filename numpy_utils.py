import numpy as np


def get_circle_filter(radius):
    diameter = 2 * radius

    quarter_filter = np.zeros((radius, radius))
    quarter_filter[:, np.arange(radius)] = np.cos(np.linspace(0., np.pi / 2., radius)) / 2.
    quarter_filter += quarter_filter.T

    right_side_filter = np.zeros((diameter, radius))
    right_side_filter[radius:] = quarter_filter
    right_side_filter[:radius] = quarter_filter[::-1, :]

    full_filter = np.zeros((diameter, diameter))
    full_filter[:, radius:] = right_side_filter
    full_filter[:, :radius] = right_side_filter[:, ::-1]
    return full_filter


def trim_zeros(arr):
    if not np.count_nonzero(arr):
        raise RuntimeError('Cannot not trim from an array of all zeros')
    dimensions = [tuple([i for i, _ in enumerate(arr.shape) if i != j])
                  for j, _ in enumerate(arr.shape)]
    empty_slices = [slice(None) for _ in arr.shape]
    for dimension, sum_axes in enumerate(dimensions):
        non_zeros = np.argwhere(np.sum(arr, axis=sum_axes) != 0).flatten()
        # First and last index from array, +1 is required to include the last non-zero index in the
        #   selection
        low, high = non_zeros[0], non_zeros[len(non_zeros) - 1] + 1
        slice_filter = empty_slices[:]
        slice_filter[dimension] = slice(low, high)
        arr = arr[slice_filter]
    return arr
