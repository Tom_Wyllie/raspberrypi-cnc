import sys

from module_finder import CNCMachine


def main():
    cnc = CNCMachine()
    valid_axes = cnc.get_valid_axes()

    # noinspection PyBroadException
    try:
        for line in sys.stdin:
            try:
                if line.startswith('#'):
                    continue
                elif line.startswith('l '):
                    laser_power = line[2:]
                    try:
                        laser_power = float(laser_power)
                    except ValueError:
                        print('Not a valid laser power: {}'.format(laser_power))
                        raise RuntimeWarning
                    else:
                        cnc.set_intensity(laser_power)
                elif line.startswith('m '):
                    # Adjust motor speed
                    line = line[2:]
                    try:
                        motor_speed = int(line)
                    except ValueError:
                        print('Not a valid motor speed: {}'.format(line))
                        raise RuntimeWarning
                    else:
                        cnc.steps_per_second = motor_speed
                else:
                    instructions = line.split()
                    if len(instructions) % 2 != 0 or not instructions:
                        raise RuntimeWarning

                    axes_steps = {}
                    for i in range(0, len(instructions), 2):
                        axis, steps = instructions[i: i+2]

                        if axis not in valid_axes:
                            raise RuntimeWarning

                        try:
                            steps = int(steps)
                            axes_steps[axis] = steps
                        except ValueError:
                            print('Not a valid step number: {}'.format(steps))
                            raise RuntimeWarning

                    cnc.axes_move_relative(axes_steps, cnc.steps_per_second)

            except RuntimeWarning:
                print('Warning: couldn\'t process line:\n{}'.format(line))
    except Exception as e:
        print('Exception occurred, aborting CNC...')
        raise e
    finally:
        print('Finishing and cleaning up...')
        cnc.cleanup()
        print('cleaned up pins')

if __name__ == '__main__':
    main()
