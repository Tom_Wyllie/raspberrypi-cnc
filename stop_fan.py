#!/usr/bin/env python
from pwm_device import PWMDevice


def main():
    fan = PWMDevice(3)
    fan.set_pwm(0)

if __name__ == '__main__':
    main()
