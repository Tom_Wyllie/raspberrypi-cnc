import threading
import time

from module_finder import GPIO


class PWMDevice:
    """Defines an object that modulates a desired pin using background threads"""
    def __init__(self, pwm_pin, frequency=1000):
        self._pwm_pin = pwm_pin
        self._pwm = 0.
        self._state = False

        # Configure pinout options
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self._pwm_pin, GPIO.OUT)

        self._thread = None
        self._thread_sentinel = False

        self._period = 1. / frequency
        self._high = None
        self._low = None

    def set_pwm(self, pwm, duration=None):
        # Interrupt any existing modulation
        if self._thread is not None:
            self._interrupt()

        self._pwm = float(pwm)

        # No need to start a new modulation if the power is set to zero
        if self._pwm == 0.:
            GPIO.output(self._pwm_pin, False)
            return

        # One PWM cycle takes self._period seconds
        self._high = self._period * self._pwm
        self._low = self._period * (1. - self._pwm)

        self._thread = threading.Thread(target=self._cycle_power_state)
        self._thread_sentinel = True
        self._thread.start()

        # If no duration is specified, leave this device modulating until self.interrupt() is called
        if duration is not None:
            # Define an interrupter thread to stop the modulation after the specified duration
            thread_interrupter = threading.Thread(target=self._interrupt, args=(duration,))
            thread_interrupter.start()

    def _interrupt(self, delay=None):
        if delay is not None:
            time.sleep(delay)
        if self._thread is not None:
            self._thread_sentinel = False
            self._thread.join()
            self._thread = None

    def _cycle_power_state(self):
        while self._thread_sentinel:
            GPIO.output(self._pwm_pin, True)
            time.sleep(self._high)
            GPIO.output(self._pwm_pin, False)
            time.sleep(self._low)

    def _set_power(self, state):
        self._state = state
        GPIO.output(self._pwm_pin, state)

    @property
    def pwm(self):
        return self._pwm

    @property
    def state(self):
        return self._state


def main():
    print('Try creating a PWM device by importing this module!')

if __name__ == "__main__":
    main()
