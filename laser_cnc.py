from cnc_controller import CNC
from module_finder import GPIO
import pwm_device

# Hardware constants
FAN_PIN = 14
LASER_PIN = 4
FAN_MAX_PWM = 1.0
FAN_MIN_PWM = 0.1


class LaserCNC(CNC):
    def __init__(self):
        super().__init__()

        # These are the pins that control the CNC
        self.motor_pins = {'x': (6, 19, 13, 26),
                           'y': (12, 20, 16, 21),
                           'z': (25, 23, 24, 22)}

        # Cooling fan for motor driver heat-sinks
        self.fan_device = pwm_device.PWMDevice(FAN_PIN)

        # Burning laser
        self.laser_device = pwm_device.PWMDevice(LASER_PIN)

        # Configure pinout options
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)

        # Setup control pins
        for pin in [pin for axis in self.motor_pins.values() for pin in axis]:
            GPIO.setup(pin, GPIO.OUT)

        # Make sure no motors are activated on initialisation
        self.release_motors()

    def get_valid_axes(self):
        return set(self.motor_pins)

    def release_motors(self):
        for pin in (pin for axis in self.motor_pins for pin in self.motor_pins[axis]):
            GPIO.output(pin, False)

        # Powering down the motors, so ensure the fan is set to the low speed
        if self.fan_device.pwm != FAN_MIN_PWM:
            self.fan_device.set_pwm(FAN_MIN_PWM)

    def _axis_step(self, axis, direction):
        """ Interacts with the pins to move the motor to the next step"""

        # Releases holding torque power from previously powered pin
        GPIO.output(self.motor_pins[axis][self._current_steps[axis] % 4], False)

        # Increments counter that keeps track of which part of the 4-phase cycle we are in
        self._current_steps[axis] += direction

        # If we are powering up pins, then ensure the fan is enabled
        if self.fan_device.pwm != FAN_MAX_PWM:
            self.fan_device.set_pwm(FAN_MAX_PWM)

        # Power next pin in phase to drive motor
        GPIO.output(self.motor_pins[axis][self._current_steps[axis] % 4], True)

    def cleanup(self):
        self.fan_device.set_pwm(0)
        self.laser_device.set_pwm(0)
        GPIO.cleanup()

    def set_intensity(self, intensity):
        super().set_intensity(intensity)
        self.laser_device.set_pwm(intensity)

    def axes_move_relative(self, steps, steps_per_second):
        if not set(self.motor_pins) >= set(steps):
            raise RuntimeError('Invalid axes specified: {}'.format(', '.join(steps.keys())))
        super().axes_move_relative(steps, steps_per_second)
