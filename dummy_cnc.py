from PIL import Image
import numpy as np

from cnc_controller import CNC
import numpy_utils

MAX_LEN = 30000
MAX_DIMS = (MAX_LEN, MAX_LEN)


class DummyCNC(CNC):
    """Defines a class that implements the same behaviour as a CNC but draws to a matrix canvas
        instead of operating the GPIO pins of the physical machine."""
    def __init__(self):
        super().__init__()
        self._canvas = np.zeros(shape=MAX_DIMS)
        # self._canvas.fill(127)

        self._origin = {'x': MAX_LEN // 2, 'y': MAX_LEN // 2, 'z': 0}
        self._head_size = 40
        self._head_filter = numpy_utils.get_circle_filter(self._head_size // 2)

    def _axis_step(self, axis, direction):
        if direction == -1:
            self._current_steps[axis] += direction

        x, y = [int(self._origin[ax] + self._current_steps[ax]) for ax in 'xy']
        head_filter = (self._intensity * self._head_filter / self._head_size)
        self._canvas[y:y + self._head_size, x:x + self._head_size] += head_filter

        if direction == 1:
            self._current_steps[axis] += direction

    def set_intensity(self, intensity):
        super().set_intensity(intensity)
        # Scale back up by performing the inverse of intensity_from_pixel in the image converter
        if intensity == 0.:
            self._intensity = 0
        else:
            self._intensity = int(255 * (intensity - 0.2) / 0.7)

    def cleanup(self):
        # Trim all unused rows and columns off the canvas
        # self._canvas = np.clip(self._canvas, 0, 1) * 255
        self._canvas = numpy_utils.trim_zeros(self._canvas)
        self._canvas = 255. - self._canvas

        self._canvas = np.asarray(self._canvas, dtype=np.uint8)
        output_image = Image.fromarray(self._canvas, 'L')
        output_image.save('dummy-cnc-plot.jpg')

    @staticmethod
    def _wait(time_to_wait):
        """Prevents the DummyCNC from ever activating time.sleep()"""
        return
