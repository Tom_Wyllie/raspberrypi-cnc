import argparse
import time
from PIL import Image

import cnc_controller


def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('bitmap', help='Bitmap file to laser etch', metavar='bitmap-file')
    arg_parser.add_argument('--step', default=40, help='Step dimensions of each pixel', metavar='step',
                            type=int)
    args = arg_parser.parse_args()

    cnc = cnc_controller.CNC()

    image = Image.open(args.bitmap, 'r').convert('L')
    pixels = list(image.getdata())
    width, height = image.size

    steps = args.step

    try:
        for row in range(height):
            for column in range(width):
                pixel = pixels[row * width + column]

                cnc.axis_move('x', steps)

                burn_time = get_burn_time_from_pixel(pixel)
                if burn_time > 0:
                    time.sleep(burn_time)

            steps *= -1
            cnc.axis_move('y', abs(steps))
    except KeyboardInterrupt as exception:
        print('Aborting laser burn')
        cnc.abort_laser()
        raise exception
    finally:
        print('Cleaning up GPIO')
        cnc.cleanup()


def get_burn_time_from_pixel(pixel):
    if pixel > 240:
        return 0
    else:
        return 1.2 - (pixel / 240.0)


if __name__ == '__main__':
    # import sys
    # sys.argv.append('bmps/circle.bmp')
    main()
