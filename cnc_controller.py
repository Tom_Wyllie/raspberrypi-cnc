import math
import time


class CNC:
    """Generic CNC class. This class should be subclassed to create different types of CNC."""
    def __init__(self):
        # Tracks how many steps have been taken on each axis at any point
        self._current_steps = {'x': 0, 'y': 0, 'z': 0}
        self._intensity = 0
        self._steps_per_second = 320

    def move_to_origin(self, steps_per_second):
        origin_steps = {axis: 0 for axis in self._current_steps}
        self.axes_move_relative(origin_steps, steps_per_second)

    def axes_move_absolute_coordinates(self, steps, steps_per_second):
        """Move the CNC to a dictionary of absolute coordinates."""
        relative_steps = {axis: steps[axis] - self._current_steps[axis] for axis in steps}
        self.axes_move_relative(relative_steps, steps_per_second)

    def axes_move_relative(self, steps, steps_per_second):
        """Move the CNC by a dictionary of coordinates relative to the current position."""
        if not all((type(steps[axis]) == int for axis in steps)):
            raise RuntimeError('Cannot handle non-integer value of steps')

        num_axes = len(steps)
        if num_axes == 0:
            return
        print('Processing steps: ', steps)

        total_euclidean_steps = math.sqrt(sum([steps[axis]**2 for axis in steps]))
        total_time = total_euclidean_steps / steps_per_second
        step_times = {axis: total_time/abs(steps[axis]) for axis in steps}
        step_directions = {axis: int(steps[axis]/abs(steps[axis])) for axis in steps}

        # Tracks steps as they occur for this event
        processed_steps = {axis: 0 for axis in steps}

        remaining_step_times = step_times.copy()
        while steps:
            # Get the next scheduled step event
            event_axis = min(remaining_step_times, key=lambda x: remaining_step_times[x])
            time_until_next_event = remaining_step_times[event_axis]

            # Wait until the next event is due
            self._wait(time_until_next_event)

            # We've now waited, so subtract the waited time off each of the remaining step times
            for axis in remaining_step_times:
                remaining_step_times[axis] -= time_until_next_event

            # The next event is now upon us
            self._axis_step(event_axis, step_directions[event_axis])

            # Track that this step has now happened
            processed_steps[event_axis] += step_directions[event_axis]

            # If the just-stepped axis now has zero steps remaining, then we're done processing
            # this axis
            if (processed_steps[event_axis] - steps[event_axis]) * step_directions[event_axis] >= 0:
                del steps[event_axis]
                del remaining_step_times[event_axis]
            else:
                # More steps remain, and having now stepped, the time until the next event on this
                # axis resets to its maximum
                remaining_step_times[event_axis] = step_times[event_axis]

    def get_valid_axes(self):
        return {'x', 'y', 'z'}  # x, y, z, are the three valid axes by default

    def _axis_step(self, axis, direction):
        raise NotImplementedError()

    def cleanup(self):
        raise NotImplementedError()

    def set_intensity(self, intensity):
        if not 0 <= intensity <= 1:
            raise ValueError('Intensity must be between 0 and 1!')
        print('Setting CNC intensity to {:.2f}'.format(intensity))
        self._intensity = intensity

    @staticmethod
    def _wait(time_to_wait):
        time.sleep(time_to_wait)

    @property
    def steps_per_second(self):
        return self._steps_per_second

    @steps_per_second.setter
    def steps_per_second(self, steps_ps):
        self._steps_per_second = steps_ps

if __name__ == "__main__":
    print('Try creating a CNC class by importing this module!')
