import numpy as np
from PIL import Image


def main():
    width, height = 64, 6
    img_matrix = np.arange(width).reshape((1, width))
    img_matrix *= int(256. / width)
    img_matrix = np.repeat(img_matrix, height, axis=0)
    img_matrix = np.asarray(img_matrix, np.uint8)

    img = Image.fromarray(img_matrix, 'L')
    img.save('grey_scale.bmp')

if __name__ == '__main__':
    main()
